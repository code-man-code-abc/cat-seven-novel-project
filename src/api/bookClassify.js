import request from './http';

// 所有频道
export function getStoreNode(params){
    return request({
        url:"/store/node.json?",
        method:"get",
        params
    })
}
// 频道下的分类
export function getStoreInfo(params){
    return request({
        url:"/store/info.json?",
        method:"get",
        params
    })
}