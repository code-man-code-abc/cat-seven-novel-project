import request from './http';

// 获取书籍评论
export function getCommentGetComments(params){
    return request({
        url:"/comment/getComments.json?",
        method:"get",
        params
    })
}