import request from './http';

// 书籍简单详情
export function getBookInfo(params){
    return request({
        url:"/book/info.json?",
        method:"get",
        params
    })
}
// 书籍详情
export function getbookGetsub(params){
    return request({
        url:"/book/getsub.json",
        method:"get",
        params
    })
}