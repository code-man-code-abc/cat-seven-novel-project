import request from './http';

// 获取章节内容
export function getReaderBookContent(params){
    return request({
        url:'/reader/book/content.json?',
        method:"get",
        params
    })
}

// 获取书籍章节
export function getBookCatalog(params){
    return request({
        url:"/book/catalog.json?",
        method:"get",
        params
    })
}