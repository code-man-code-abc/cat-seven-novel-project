import request from './http';

// 热门搜索
export function getSearchGetHotWords(params){
    return request({
        url:"/search/getHotWords?",
        method:"get",
        params
    })
}

// 搜索接口
export function getSearchSearchHint(params){
    return request({
        url:"/search/searchHint?",
        method:"get",
        params
    })
}