import axios from "axios";

// http://192.168.110.2:3000/banner
const service = axios.create({
    baseURL:"",
    // baseURL:"https://apis.netstart.cn/yunyuedu",
    timeout:30000,//请求超时时间
    withCredentials:true,// 自动获取后台服务器cookie同步本地
    headers:{
        "Content-Type":"application/json;charset=UTF-8" //表单数据类型
    },
})

// request  拦截器 发送数据到后台服务器前拦截
service.interceptors.request.use(
    config=>{
        return config;
    },
    error=>{
        console.log("axios中request报错",error);
        Promise.reject(error);
    }
)

// response  拦截器 获取后台数据前拦截
service.interceptors.response.use(
    response=>{
        return response.data;
    },
    error=>{
        console.log("axios中response报错",error);
        Promise.reject(error);
    }
)

export default service;
