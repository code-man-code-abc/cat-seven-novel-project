import request from "./http";

// 书城导航
export function getStoreNavi(params){
    return request({
        url:"/store/navi.json?",
        method:"get",
        params
    })
}
// 书城展示模块
export function getStoreShow(params){
    return request({
        url:"/store/show.json?",
        method:"get",
        params
    })
}