import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bookIDList:["1faa487a443d4b8badfcbb6ded50b844_4","f23ec8de08f2431d98df633ad7df9dd9_4","927e1bf744ae4ebfb9642a33057409bf_4","bd_a0a5215b23734d4aa516987e65ce07dc_4","bd_c07dea43969449a3a65158b96aab04c2_4"], // 获取添加书架id
    bookHistoryList:[], // 获取历史记录
    chapterIndex:null, // 目录点击的章节下标
  },
  mutations: {
    // 添加书架
    addbookData(state,data){
      state.bookIDList.push(data)
    },
    // 添加历史记录
    addHistoryData(state,data){
      let index = state.bookHistoryList.findIndex(item =>item == data);
      if(index != -1){
        state.bookHistoryList.splice(index,1);
        state.bookHistoryList.unshift(data);
      }else{
        state.bookHistoryList.unshift(data)
      }
    },
    // 删除书架上的书
    delbookData(state,data){
      let index = state.bookIDList.findIndex(item =>item == data);
      state.bookIDList.splice(index,1)
    },
    // 获取下标
    getChapterFun(state,data){
      state.chapterIndex = data;
    }
  },
  actions: {
  },
  modules: {
  }
})
