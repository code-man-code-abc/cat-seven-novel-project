import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
// import bookrack from '../views/bookrack/bookrack.vue'
import Login from '../views/Login.vue'
// import bookCity from '../views/bookCity/bookCity.vue'


Vue.use(VueRouter)

// const routes = [
//   {
//     path: '/',
//     redirect:"/bookrack/bookrack"
//   },
//   {
//     path: '/bookrack/bookrack',
//     name: 'bookrack',
//     component: bookrack
//   },
const routes = [
      // {
      //   path: '/',
      //   redirect:"/bookrack/bookrack"
      // },
      // {
      //   path: '/bookrack/bookrack',
      //   name: 'bookrack',
      //   component: bookrack
      // },

    {
      path: '/',
      redirect:"/Login"
    },
    {
      path: '/Login/Login',
      name: 'Login',
      component: Login
    },

  // {
  //   path: '/',
  //   redirect:"/bookCity/bookCity"
  // },
  // {
  //   path: '/bookCity/bookCity',
  //   name: 'bookCity',
  //   component: bookCity
  // },

    {
      path: '/bookCity/bookCity',
      name: 'bookCity',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/bookCity/bookCity.vue')
    },
    {
      path: '/bookrack/bookrack',
      name: 'bookrack',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ '../views/bookrack/bookrack.vue')
    },
    {
    path:'/bookClassify/bookClassify',
    name:'bookClassify',
    component:() => import('../views/bookClassify/bookClassify.vue')
  },
  {
    path:'/my/my',
    name:'my',
    component:() => import('../views/my/my.vue')
  },
  {
    path:'/mustRead/mustRead',
    name:'mustRead',
    component:() => import('../views/mustRead/mustRead.vue')
  },
  {
    path:'/mustRead/mustReadRankList',
    name:'mustReadRankList',
    component:() => import('../views/mustRead/mustReadRankList.vue')
  },
  {
    path:'/search/search',
    name:'search',
    component:() => import('../components/search/search.vue')
  },
  {
    path:'/writerPage',
    name:'writerPage',
    component:() => import('../views/writerPage.vue')
  },
  {
    path:'/TheWorkDetails',
    name:'TheWorkDetails',
    component:() => import('../views/TheWorkDetails.vue')
  },
  {
    path:'/catalogueList',
    name:'catalogueList',
    component:() => import('../views/catalogueList.vue')
  },
  {
    path:'/bookReviewList',
    name:'bookReviewList',
    component:() => import('../views/bookReviewList.vue')
  },
  {
    path:'/member',
    name:'member',
    component:() => import('../views/member.vue')
  },
  {
    path:'/readHistory',
    name:'readHistory',
    component:() => import('../views/readHistory.vue')
  },
  {
    path:'/CategoryPages',
    name:'CategoryPages',
    component:() => import('../views/CategoryPages.vue')
  },
  {
    path:'/ReadThePage',
    name:'ReadThePage',
    component:() => import('../views/ReadThePage.vue')
  },
  {
    path:'/Login',
    name:'Login',
    component:() => import('../views/Login.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
