import Vue from 'vue'
import Vant from 'vant';
import App from './App.vue'
import router from './router'
import store from './store'


// https://vant-contrib.gitee.io/vant/#/zh-CN/quickstart
// $cnpm i vant -S 
// $cnpm i babel-plugin-import -D
import 'vant/lib/index.css';
Vue.use(Vant);

Vue.config.productionTip = false

// 懒加载
import { Lazyload } from 'vant';
Vue.use(Lazyload);

// 引入字体图标
import "./assets/font/iconfont.css"

// 安装axios
// $cnpm i axios -S
// 插件 滚动效果
//$cnpm i swiper@3.4.2 -S
import "swiper/dist/css/swiper.min.css"


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
